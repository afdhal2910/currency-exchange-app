package com.emg.currencyexchangeapp.network;

import com.emg.currencyexchangeapp.model.MoneyCurrency;

import java.util.Currency;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    @GET("latest?base=USD")
    Call<MoneyCurrency> getCurrency();
}
