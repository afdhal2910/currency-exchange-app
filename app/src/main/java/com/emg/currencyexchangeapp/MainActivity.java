package com.emg.currencyexchangeapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.emg.currencyexchangeapp.model.MoneyCurrency;
import com.emg.currencyexchangeapp.network.Api;
import com.emg.currencyexchangeapp.network.ApiClient;
import com.orhanobut.hawk.Hawk;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements  AdapterView.OnItemSelectedListener {

    String[] country = { "CANADA", "INDONESIA", "EURO", "MALAYSIA", "AUSTRALIA"};
    Api apiInterface;
    String mCanada, mIndonesia, mEuro, mMalaysia, mAustralia;
    TextView currencyResult;
    EditText currencyValue;
    Button btn;
    String selectedCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currencyResult = (TextView)findViewById(R.id.currency_result);
        btn = (Button) findViewById(R.id.btn);
        currencyValue = (EditText) findViewById(R.id.currency_value);
        Spinner spin = (Spinner) findViewById(R.id.spinner);
        spin.setOnItemSelectedListener(this);

        apiInterface = ApiClient.getClient().create(Api.class);

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        if (InternetConnection.checkConnection(this)) {
            getUpdateCurrency();
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value= currencyValue.getText().toString();
                if (value.trim().equals("")) {
                    Toast.makeText(MainActivity.this, "Please insert your number ", Toast.LENGTH_SHORT).show();
                    return;
                }else{

                    String canada = Hawk.get("CAD");
                    String indonesia = Hawk.get("INR");
                    String euro = Hawk.get("EUR");
                    String malaysia = Hawk.get("MYR");
                    String australia = Hawk.get("AUD");
                    double result = 0.0;

                    mCanada = canada;
                    mIndonesia = indonesia;
                    mEuro = euro;
                    mMalaysia = malaysia;
                    mAustralia = australia;

                    int finalValue= 0;
                    finalValue = Integer.parseInt(value);
                    if(selectedCountry == "CANADA"){
                        result = finalValue * Double.parseDouble(mCanada);
                        currencyResult.setText("CAD " + result);

                    }else if(selectedCountry == "INDONESIA"){
                        result = finalValue * Double.parseDouble(mIndonesia);
                        currencyResult.setText("INR " + result);

                    }else if(selectedCountry == "EURO"){
                        result = finalValue * Double.parseDouble(mEuro);
                        currencyResult.setText("EUR " + result);

                    }else if(selectedCountry == "MALAYSIA"){
                        result = finalValue * Double.parseDouble(mMalaysia);
                        currencyResult.setText("MYR " + result);

                    }else if(selectedCountry == "AUSTRALIA"){
                        result = finalValue * Double.parseDouble(mAustralia);
                        currencyResult.setText("AUD " + result);
                    }
                }
            }
        });
    }

    private void getUpdateCurrency() {
        Call<MoneyCurrency> call = apiInterface.getCurrency();
        call.enqueue(new Callback<MoneyCurrency>() {
            @Override
            public void onResponse(Call<MoneyCurrency> call, Response<MoneyCurrency> response) {
                if(response.isSuccessful()){
                    Hawk.put("CAD", response.body().getRates().getAUD().toString());
                    Hawk.put("INR", response.body().getRates().getIDR().toString());
                    Hawk.put("EUR", response.body().getRates().getEUR().toString());
                    Hawk.put("MYR", response.body().getRates().getMYR().toString());
                    Hawk.put("AUD", response.body().getRates().getAUD().toString());
                }
            }

            @Override
            public void onFailure(Call<MoneyCurrency> call, Throwable t) {
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        selectedCountry = country[i];
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public static class InternetConnection {
        /**
         * CHECK WHETHER INTERNET CONNECTION IS AVAILABLE OR NOT
         */
        public static boolean checkConnection(Context context) {
            final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connMgr != null) {
                NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

                if (activeNetworkInfo != null) { // connected to the internet
                    // connected to the mobile provider's data plan
                    if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                        // connected to wifi
                        return true;
                    } else return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
                }
            }
            return false;
        }
    }
}