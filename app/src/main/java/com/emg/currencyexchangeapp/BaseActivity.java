package com.emg.currencyexchangeapp;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.orhanobut.hawk.Hawk;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Hawk.init(this).build();
        Hawk.put("CAD", "0");
        Hawk.put("IND", "0");
        Hawk.put("EUR", "0");
        Hawk.put("MYR", "0");
        Hawk.put("AUD", "0");
    }
}
