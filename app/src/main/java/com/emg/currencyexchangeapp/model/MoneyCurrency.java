package com.emg.currencyexchangeapp.model;

import com.google.gson.annotations.SerializedName;

public class MoneyCurrency {
    @SerializedName("base")
    public String base;

    @SerializedName("date")
    public String date;

    @SerializedName("rates")
    Rates rates;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Rates getRates() {
        return rates;
    }

    public void setRates(Rates rates) {
        this.rates = rates;
    }
}
