package com.emg.currencyexchangeapp.model;

import com.google.gson.annotations.SerializedName;

public class Rates {

    @SerializedName("CAD")
    public Float CAD;

    @SerializedName("IDR")
    public Float IDR;

    @SerializedName("EUR")
    public Float EUR;

    @SerializedName("MYR")
    public Float MYR;

    @SerializedName("AUD")
    public Float AUD;

    public Float getCAD() {
        return CAD;
    }

    public void setCAD(Float CAD) {
        this.CAD = CAD;
    }

    public Float getIDR() {
        return IDR;
    }

    public void setIDR(Float IDR) {
        this.IDR = IDR;
    }

    public Float getEUR() {
        return EUR;
    }

    public void setEUR(Float EUR) {
        this.EUR = EUR;
    }

    public Float getMYR() {
        return MYR;
    }

    public void setMYR(Float MYR) {
        this.MYR = MYR;
    }

    public Float getAUD() {
        return AUD;
    }

    public void setAUD(Float AUD) {
        this.AUD = AUD;
    }


}
